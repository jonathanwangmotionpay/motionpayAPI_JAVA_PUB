



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
function imageSel(imageName) {
	var radioButtonEle = document.getElementById("bank_" + imageName);
	if(radioButtonEle) {
		radioButtonEle.checked = true;
	}
}
function validationForm(thisForm) {
	var radioButton1 = document.getElementById("bank_SCAN_ALIPAY");
	var radioButton2 = document.getElementById("bank_WX_SCANQRCODEPAY");
	var radioButton3 = document.getElementById("bank_H5_ALIPAY");
	var radioButton4 = document.getElementById("bank_H5_WECHAT");
	var radioButton5 = document.getElementById("bank_SM_ALIPAY");
	var radioButton6 = document.getElementById("bank_SM_WECHAT");
	var radioButton7 = document.getElementById("bank_SM_PU");
	var radioButton8 = document.getElementById("bank_QR_PA");
	var radioButton9 = document.getElementById("bank_QR_PW");

	if (radioButton1.checked == false && radioButton2.checked == false 
			&& radioButton3.checked == false && radioButton4.checked == false
			&& radioButton5.checked == false && radioButton6.checked == false
			&& radioButton7.checked == false && radioButton8.checked == false
			&& radioButton9.checked == false) {
		alert("Please select one payment method.");
	}
	else {
		document.getElementById("payment").submit();
	}	
}
</script>	

  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span >Sample Payment Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>     
      <span style="float:right;"><a href="orderList.jsp"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>
  
  <form id="payment" action="payment.jsp" method="post" >
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Gateway</label></h6>
  		<label>Final Price: <font class="cOrange" style="font-size: 25px;">CAD$0.01</font></label>
 	  	<label>Other Amount: <input type="text" name="paymentAmount" value="0.01" size="5"/></label> <br/>
  	  <ul class="sel_list">
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_SCAN_ALIPAY" value="A" />
               <img src='images/alipay.png'/>
             </label>
           </li>
         
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_WX_SCANQRCODEPAY" value="W" />
               <img src='images/wechatpay.png'/>
             </label>
           </li>

      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_H5_ALIPAY" value="H5_A" />
               <img src='images/alipayH5.png'/>
             </label>
           </li>
         
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_H5_WECHAT" value="H5_W" />
               <img src='images/wechatH5.png'/> (API Fixing)
             </label>
           </li>
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_SM_WECHAT" value="SM_A" />
               <img src='images/scanMobileA.png'/> (with server)
             </label>
           </li>               
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_SM_ALIPAY" value="SM_W" />
               <img src='images/scanMobileW.png'/> (with server)
             </label>
           </li>         
            
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_SM_PU" value="SM_PU" />
               <img src='images/scanMobilePU.png'/> (POS without server)
             </label>
           </li>                    

      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_QR_PA" value="QR_PA" />
               <img src='images/alipayQR_PA.png'/> (POS without server)
             </label>
           </li>
         
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentInfo.bankSelect" id="bank_QR_PW" value="QR_PW" />
               <img src='images/wechatQR_PW.png'/> (POS without server)
             </label>
           </li>
         
      </ul>
      <div class="confirm_pay">
      	<button class="greenbutton" onclick="validationForm(this);">Pay Now</button>
      </div>
    </div>
  </form>   
 
</div>

</body>

</html>


