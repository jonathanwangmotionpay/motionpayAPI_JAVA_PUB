<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html data-dpr="1" style="font-size: 54px;">
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no" />
<title>Motion Pay</title>
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-status-bar-style"
	content="black-translucent">
<meta name="apple-touch-fullscreen" content="yes">
<script src="js/flexible.js"></script>
<link rel="stylesheet" href="styles/css/wechatpay.css">
<link rel="stylesheet" href="styles/css/weui.min.css">
<script src="js/jweixin-1.1.0.js"></script>
<script src="js/js/jquery-1.8.2.min.js"></script>
<script src="js/md5.js"></script>
<script type="text/javascript" id="selfjs">
var maxTime = 5000; // 最大查询订单时间5秒钟
var curTime = 0;
var aSelIdOld="";

$(function(){
	rmbAmount();
	
	$(".atip a").each(function(index, element) {
		$(this).click(function(){
			$(this).addClass("cur").siblings("a").removeClass("cur");
		})
	});
	
	wx.config({
		debug: false,
		appId: "<%= (String)request.getSession().getAttribute("appId") %>",
		jsApiList: ['checkJsApi','chooseWXPay']
	});
	wx.ready(function(res){
		wx.checkJsApi({
			jsApiList: ['chooseWXPay'],
			success: function(res) {
				if(!res.checkResult.chooseWXPay){
					showDiv('当前微信版本不支持所需操作，请更新版本。');
					return false;
				}
			}
		});
	});
})

// 支付提交
function submit() {
	$("#pay").removeClass("active");
	var amount = $("#amt").val();
	// alert("postToURL:" + 'https://demo.motionpay.org/motionpayAPI_JAVA/WeChatPayOfficalAccountService');
	ajaxPost('https://demo.motionpay.org/motionpayAPI_JAVA/WeChatPayOfficalAccountService',{
		"mid": "<%= (String)request.getSession().getAttribute("mid") %>",
		"fid": "<%= (String)request.getSession().getAttribute("fid") %>",
		"amount": parseInt(amount),
	},function(dataStr){
		// alert("dataStr is:"+dataStr);
		var data = JSON.parse(dataStr);
		// var data = dataStr; we have to convert it.
		// alert("code is:"+data.code);
		if (data.code == 0) {
			var content=data.content;
            var paymentPackage = "prepay_id=" + content.prepay_id;
            var timeStamp = content.timeStamp;
            var signType = content.signType;
            var nonceStr = content.nonce_str;
            var paySign = content.paySign;
			// alert("paymentPackage is:"+paymentPackage);
			wx.chooseWXPay({
                   "timestamp": timeStamp,
                   "nonceStr": nonceStr,
                   "package": paymentPackage,
                   "signType": signType,
                   "paySign": paySign,
                   success: function (res) {
					   // alert("success!");
                       // 支付成功后的回调函数
                   	if(res.errMsg == "chooseWXPay:ok" ) {
                   		showDiv("支付成功");
                   		setTimeout(function(){
                   			window.location.reload();
                   		}, 2000);
                       }
                       else if(res.errMsg == "chooseWXPay:fail"){
                       	showDiv("支付失败");
                       }
                       else if(res.errMsg == "chooseWXPay:cancel"){
                       	showDiv("已取消支付");
                       }
                       else{
                       	showDiv("支付访问被拒绝:商户设置错误");
                       }
                   },
				fail:function(res){
					// alert("fail!");
					showDiv(res.errMsg);
				}
               });
		} else if (data.code == -1){
			// alert("code is: -1");
			showDiv(data.message);
		} else if (data.code == -2){
			// alert("code is: -2");
			showDiv(data.message);
			wx.closeWindow();
		} else {
			// alert("code is something else.");
			showDiv(data.message);
		}
	});
}
function ajaxGet(url,callback){
	$.ajax({
		type : "GET",
		url : url,
		dataType : 'json',
		cache : false,
		beforeSend: loading,
		success : function(data) {
			unLoading();
			callback(data);
		},
		error : function(xhr, status) {
			//_ajaxErrorHandler(xhr);
			unLoading();
			showDiv("支付异常"); 
		}
	});
}
function ajaxPost(url,data,callback){
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			unLoading();
			callback(data);
		},
		error : function(xhr, status) {
			//_ajaxErrorHandler(xhr);
			unLoading();
			showDiv("支付异常"); 
		}
	});
}
function loading(){
 	$("#divLoading").css('display','block');
}
function unLoading(){
	$("#divLoading").css('display','none');
}
function showDiv(msg) {
	$("#popMsg").html(msg);
	document.getElementById('popDiv').style.display = 'block';
}
function closeDiv() {
	document.getElementById('popDiv').style.display = 'none';
}
function rmbAmount() {
	var rate="${rate}";
	var amount=$("#amount").html();
	if(amount.trim()==""){
		amount=0;
	}
	var totalAmt=0;
	var rmbAmountVal=0;
	rmbAmountVal=amount;
	rmbAmountVal=rmbAmountVal*rate;
	rmbAmountVal=rmbAmountVal.toFixed(2);
	$("#rmbAmount").html(rmbAmountVal);
	setTimeout(function(){rmbAmount();},500);
}
</script>
</head>
<body style="font-size: 12px;">
	<!-- loading 图片 -->
	<div id="divLoading"
		style="display: none; position: absolute; width: 100%; height: 100%; z-index: 99999; background: #000; text-align: center; padding-top: 50%; filter: alpha(opacity = 50); opacity: 0.5; -moz-opacity: 0.5; opacity: 0.5;">
		<img src="images/ajax-loader.gif" alt="loading..."
			width="30" />
		<p style="color: #ccc; font-size: 0.6rem;">loading...</p>
	</div>
	<!-- 提示对话框 -->
	<div id="popDiv"
		style="display: none; background: rgba(52, 52, 52, .5); z-index: 9999; width: 100%; height: 100%; position: fixed; top: 0px; left: 0px;">
		<div
			style="background: #fff; z-index: 20; width: 70%; position: absolute; left: 15%; top: 30%; border-radius: 15px;">
			<p
				style="padding: 0.5rem 10px; text-align: center; font-size: 1.2em; border-bottom: 1px #eaeaea solid;"
				id="popMsg" name="popMsg">提示</p>
			<div
				style="line-height: 1.2rem; height: 1.2rem; width: 100%; font-size: 1.2em;">
				<a data-role="none"
					style="width: 100%; height: 1.2rem; line-height: 1.2rem; display: block; text-align: center; font-size: 1.2em; color: #f2780b;"
					id="popURL" href="#" onClick="closeDiv()">确&nbsp&nbsp定</a>
			</div>
		</div>
	</div>
	<div id="wrap">
		<div id="front">
			<header>
				<div class="logo">
					<img src="images/Motionpay-Logo.gif">
					<h1>MotionPay Demo</h1>
				</div>
			</header>
			<section>
				<div class="amount" style="margin-bottom: 0.3rem;">
					<span style="font-size: 15px;">金额</span>
					<p>
						$ <span id="amount">0.00</span>
					</p>
					<input type="hidden" id="amt" value="$0.00">
				</div>
				<!-- <div class="ratebox">
		    <div>汇率:$1≈￥<span>${rate}</span></div>
		    <p style="padding-right:0.3rem;">≈￥<span id="rmbAmount">0.00</span></p>
		</div> -->
				<h3>
					<span>Powered by Motion Pay</span>
				</h3>
			</section>
			<footer>
				<div id="keyBoard" class="show-trans show">
					<i data-str="1">1</i><i data-str="2">2</i><i data-str="3">3</i><i
						data-str="del"><img
						src="images/backspace.png"></i> <i
						data-str="submit" id="pay">微信<br>支付
					</i> <i data-str="4">4</i><i data-str="5">5</i><i data-str="6"
						class="no-right">6</i> <i data-str="7">7</i><i data-str="8">8</i><i
						data-str="9" class="no-right">9</i> <i data-str="hide" id="hide"><img
						src="images/hide_keypad.png"></i><i
						data-str="0">0</i><i data-str="." class="no-right"><span>.</span></i>
				</div>
			</footer>
		</div>
	</div>
	<script src="js/lib-pay.js"></script>
</body>
</html>