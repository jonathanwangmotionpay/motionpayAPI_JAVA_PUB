/**
 * Offical Account Payment 
 */
package com.motionpaytech.pay.bean;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import com.motionpaytech.fastjson.JSON;
import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.util.SignSHA1Util;

/**
 * Servlet implementation class WeChatPayOfficalAccountService
 * author Jonathan
 * WeChatPay Official Account Service
 */
@WebServlet("/WeChatPayOfficalAccountService")
public class WeChatPayOfficalAccountService extends HttpServlet {

	/**
	 * Serialized version id
	 */
	private static final long serialVersionUID = 1L;
	private static boolean loggerCreated = false;
	private static final String LOG_FILENAME = AppConfigInitUtil.INSTANCE.getTomcatFolder() + "logs/WeChatPayOfficalAccountService" + 
			   AppConfigInitUtil.INSTANCE.getTomcatWebAppName() + ".log";

	private static final String BASE_URL = "https://guigu.wizarpos.com/onlinePayment/v1_1/pay";
	private static final String ORDER_OFFICAL_ACCOUNT_URL = BASE_URL + "/jsapi";

    /* CNY parameters live 
    private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID = ("100100010000056");
	private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPID = ("5005642018020");
	private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPSECURE = ("100d4c12c43df16eca30ae17e75935b2"); */
	
    /* CAD parameters  live 
	private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID = ("100100010000055");
	private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPID = ("5005642018019");
	private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPSECURE = ("c7d79e0075076e72c5f779fc62f0f4e9"); */
	
    /* CAD parameters  test */
	public static final String WECHAT_OFFICAL_ACCOUNT_APPID = ("wx8ddacf4e1347f9c8");
	public static final String WECHAT_OFFICAL_ACCOUNT_APPSECURE = ("565ccc5a1fdb8e9873b7244b4b044935");
	public static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID = ("100122220000002");
	private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPID = ("5005642018006");
	private static final String WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPSECURE = ("318e5d91d886a034bf0b16cdafd7134a");	
		
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WeChatPayOfficalAccountService() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private void createLogger() {
    	if (loggerCreated == true)
    		return;
    	try {
	    	Logger logger = Logger.getLogger("WeChatPayOfficalAccountServiceLog");
	        FileHandler fh = new FileHandler(LOG_FILENAME, true);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);
	        logger.info("WeChatPayOfficalAccountServiceLog init");       

	        loggerCreated = true;
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    //Initialize global variables
    public void init() throws ServletException {
    	createLogger();
    }
 	
	private String getSign(String jsonStr) {
        final JSONObject object = JSON.parseObject(jsonStr);
        object.put("sign", "");
        // System.out.println("the json obj is:"+object.toJSONString());
        final String signStr = SignSHA1Util.getSignOnline(object, WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPID, WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPSECURE);
        return signStr;
	}
	
	private String generateOutTradeNo()
	{
		String str = new SimpleDateFormat("yyyyMMdd").format(new Date()) + Math.round(Math.random() * 89999);
		return str;
	}
	
	public JSONObject subMerchantPrePayCanada(JSONObject inputJson) {
		String out_trade_no = generateOutTradeNo();
		String total_fee = inputJson.getString("tranAmount");
		String openid = inputJson.getString("openId");
		
		total_fee =  "1";

		System.out.println("The openid in Json:" + openid);
		JSONObject requestJson = new JSONObject();
		requestJson.put("mid", WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID);
		requestJson.put("out_trade_no", out_trade_no);
		requestJson.put("pay_channel", "W");
		requestJson.put("total_fee", total_fee);
		requestJson.put("goods_info", "ThirdPartyProduct");
		requestJson.put("return_url",  AppConfigInitUtil.INSTANCE.getDefaultReturnURL());
		requestJson.put("spbill_create_ip", "192.168.1.132");
		// requestJson.put("terminal_no", "OfficalAccount"); // Official Account later
		requestJson.put("terminal_no", "WebServer"); // Use WebServer for now.
		
		requestJson.put("openid", openid);
//		SysWxChildMerchantToken token=sysWxChildMerchantTokenDao.getByMid(prePayReq.getSysParam().getMid());
//		String sign = MakeSign.getSignValue(inputJson, token.getWxAppId(), token.getWxAppSecret());
//		String sign = MakeSign.getSignValue(inputJson, "5005642018003", "a0e99280e3c9b4df6f907c7eab3e063e");
		String signLocal = getSign(requestJson.toJSONString());
		requestJson.put("sign", signLocal);
		
		//send payment request
		System.out.println("The input Json:" + requestJson);
		String result = postResult(ORDER_OFFICAL_ACCOUNT_URL, requestJson);
		JSONObject resultJson = JSON.parseObject(result);
		System.out.println("The response Json:" + result);
		return resultJson;
	}
	
	public static String postResult(String url, JSONObject requestJson) {
		String requestParam = JSON.toJSONString(requestJson);
		String strResult = sendPost(url, requestParam);
		return strResult;
	}
	
	public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            URLConnection conn = realUrl.openConnection();

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "utf-8");  

            conn.setDoOutput(true);
            conn.setDoInput(true);

            out = new PrintWriter(conn.getOutputStream());
            out.print(param);

            out.flush();

            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("Send POST response Exception:"+e);
            e.printStackTrace();
        }

        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }    
    
    //页面js调用   后台调用预下单接口，获取支付参数
    public String subMerchantPay(
    		HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws Exception {
    	String res = "";
    	Logger logger = Logger.getLogger("WeChatSubMerchantPayLog");
    	try {
    		String openId = (String) httpRequest.getSession().getAttribute("openId");
    		logger.info("openId:"+openId);
    		JSONObject jsonRequest = new JSONObject(); 
    		JSONObject resJson=subMerchantPrePayCanada(jsonRequest);
    		if("0".equals(resJson.getString("code"))){
    			JSONObject content=resJson.getJSONObject("content");
    			//content 里包含prepay_id等页面调用微信支付所需的参数
    			res= content.toJSONString();
    		} else {
    			res = (resJson.getString("message"));
    		}
    	} catch (Exception e) {
    		res= ("failed");
    	}
    	return res;
    }    
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Logger logger = Logger.getLogger("WeChatPayOfficalAccountServiceLog");
		logger.info("get post request.");
		response.getWriter().append("WeChatPayOfficalAccountService doesn't support get!");
	}
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Logger.getLogger("WeChatPayOfficalAccountServiceLog");		
		try {
            String line = "";
            String inputStr = "";
            logger.info("get post request in WeChatPayOfficalAccountService servlet.");
            BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));

            while ((line = reader.readLine()) != null) {
            	inputStr += line;
            }
            logger.info("the contenet we got:" + inputStr);
            
            HttpSession session = request.getSession();
            session.setAttribute("TestParam", "empty");    		
            Enumeration<String> keys = session.getAttributeNames();
            while (keys.hasMoreElements())
            {
              String key = (String)keys.nextElement();
              logger.info(key + ": " + session.getAttribute(key) + "<br>");
            }
            
    		String openId = (String) request.getSession().getAttribute("openId");
            JSONObject jsonRequest = new JSONObject();    
            jsonRequest.put("content", inputStr);
            jsonRequest.put("openId", openId);
			if(jsonRequest.isEmpty() != true) {
				JSONObject jsonResponse = subMerchantPrePayCanada(jsonRequest);			
				String es = jsonResponse.toJSONString();
				logger.info("the contenet in response:" + es);
				response.getWriter().write(es);	
			}
		    
            logger.info("exit the servlet");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.info("exception in WeChatPayOfficalAccountService:+" + e.getMessage());
		}			
	}
}
