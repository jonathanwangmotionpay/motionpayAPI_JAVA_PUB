package com.motionpaytech.pay.bean.test;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.onlineapi.presenter.model.req.PayOnlineMobileScanReq;
import com.motionpaytech.onlineapi.util.TokenGenerater;
import com.motionpaytech.pay.bean.utils.HttpMethod;
import com.motionpaytech.pay.bean.utils.RSAUtil;
import com.motionpaytech.qrgen.javase.QRCode;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;


/**
* @ClassName: PayTest
* @Description: Generate QR code for scan and payment
* @author jonathan.wang@motionpay.ca
* @date 2017-08-09 16:19:15
*
*/
public class PrePayTest {
	
	/**
	 * Encryption method name
	 * */
	private static final String ALGORITHOM = "RSA";
	/**
	 * default encryption provider
	 * */
	private static final Provider DEFAULT_PROVIDER = new BouncyCastleProvider();
	
	public static void main(String[] args) throws Exception {
    
	}
	
	public static void oldMain(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException {
		
	}
}
