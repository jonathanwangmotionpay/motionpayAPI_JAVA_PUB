package com.motionpaytech.pay.bean.test;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.pay.bean.utils.HttpMethod;
import com.motionpaytech.pay.bean.utils.RSAUtil;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;


/**
* @ClassName: PayTest
* @Description: 条码支付
* @author jonathan
* @date 2017年5月25日 下午6:19:15
*
 */
public class OrderPayTest {
	
	/**
	 * 算法名称
	 * */
	private static final String ALGORITHOM = "RSA";
	/**
	 * 默认的安全服务提供者
	 * */
	private static final Provider DEFAULT_PROVIDER = new BouncyCastleProvider();
	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException {

	}
}
