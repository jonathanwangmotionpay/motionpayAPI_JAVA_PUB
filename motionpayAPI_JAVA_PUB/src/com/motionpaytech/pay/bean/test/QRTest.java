package com.motionpaytech.pay.bean.test;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import com.motionpaytech.qrgen.javase.QRCode;

/**
* @ClassName: PayTest
* @Description: Generate QR code Image from URL
* @author jonathan.wang@motionpay.ca
* @date 2017-08-09 16:19:15
*
*/
public class QRTest {
	

    public void shouldColorOutput() throws IOException {
        File file = QRCode.from("http://motionpay.ca/").withColor(0x00000000, 0xFFFFFFFF).file();
        File tempFile = File.createTempFile("qr_", ".png");
        Files.copy(file.toPath(), new FileOutputStream(tempFile));
        System.out.println(tempFile.getAbsoluteFile());
    }
    
    public void generateQRcode(String url, String filename)  throws IOException {
        File file = QRCode.from(url).withColor(0x00000000, 0xFFFFFFFF).file();
        File tempFile = new File(filename);
        Files.copy(file.toPath(), new FileOutputStream(tempFile));
        // System.out.println(tempFile.getAbsoluteFile());    	
    }
    
    public static void main(String[] args) {
    	QRTest t = new QRTest();
    	try {
    		t.generateQRcode("http://motionpay.ca/", "c:\\temp\\qrcode.png");
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
}
