package com.motionpaytech.pay.bean;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.motionpaytech.fastjson.JSON;
import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.util.SignSHA1Util;

/**
 * Servlet implementation class WeChatSubMerchantPay
 * author Jonathan
 * get call back for payment result
 */
@WebServlet("/subMerchantPay")
public class WeChatSubMerchantPay extends HttpServlet {

	/**
	 * Serialized version id
	 */
	private static final long serialVersionUID = 1L;
	private static boolean loggerCreated = false;
	private static final String LOG_FILENAME = AppConfigInitUtil.INSTANCE.getTomcatFolder() + "logs/WeChatSubMerchantPay" + 
			   AppConfigInitUtil.INSTANCE.getTomcatWebAppName() + ".log";
	
	
	private static final String WX_RED_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={APPID}&redirect_uri={SCHEME}%3a%2f%2f{DNS}%2fmotionpayAPI_JAVA%2f{ADDR}&response_type=code&scope=snsapi_base&state={STATE}#wechat_redirect";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WeChatSubMerchantPay() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private void createLogger() {
    	if (loggerCreated == true)
    		return;
    	try {
	    	Logger logger = Logger.getLogger("WeChatSubMerchantPayLog");
	        FileHandler fh = new FileHandler(LOG_FILENAME, true);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);
	        logger.info("WeChatSubMerchantPayLog init");       

	        loggerCreated = true;
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    //Initialize global variables
    public void init() throws ServletException {
    	createLogger();
    }

    public JSONObject httpSendGetRequest(String urlToRead) {
    	Logger logger = Logger.getLogger("WeChatSubMerchantPayLog");
    	 JSONObject json = null;
         StringBuilder result = new StringBuilder();
         try {
	         URL url = new URL(urlToRead);
	         HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	         conn.setRequestMethod("GET");
	         BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	         String line;
	         while ((line = rd.readLine()) != null) {
	            result.append(line);
	         }
	         rd.close();
	         json = JSONObject.parseObject(result.toString());
         }
         catch(Exception e) {
        	 logger.info("httpSendGetRequest exception:"+e);
         }
    	 return json;
    }
  
    
    public String getOpenId(String code) {
    	Logger logger = Logger.getLogger("WeChatSubMerchantPayLog");
	    String openId = null;
	    String appId = WeChatPayOfficalAccountService.WECHAT_OFFICAL_ACCOUNT_APPID;
	    String appSecret = WeChatPayOfficalAccountService.WECHAT_OFFICAL_ACCOUNT_APPSECURE;
	    StringBuffer url = new StringBuffer();
	    
	      url.append("https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code");
	      url.append("&appid=");
	      url.append(appId);
	      url.append("&secret=");
	      url.append(appSecret);
	      url.append("&code=");
	      url.append(code);
		    
	    JSONObject resJson = httpSendGetRequest(url.toString());
	    if ((resJson == null) || (!resJson.containsKey("openid")))
	    {
	      if (resJson != null) {
	        logger.info("Json Result has values but has no openID:" + resJson.toJSONString());
	      }
	      logger.info("Json Result is null or no openid:" + resJson.toJSONString());
	    }
	    openId = resJson.getString("openid");
	    return openId;	    
    }
    	    
  

    //跳转支付页面
    //redirect to the payment JSP page
    public String subMerchantPay(String mid,
    		HttpServletRequest request, HttpServletResponse response) {
    	Logger logger = Logger.getLogger("WeChatSubMerchantPayLog");
    	String openId=(String) request.getSession().getAttribute("openId");
    	String appId = WeChatPayOfficalAccountService.WECHAT_OFFICAL_ACCOUNT_APPID;
    	String code = request.getParameter("code");
    	logger.info("code is:" + code);
    	if(code != null && code.length() > 0) {
        	request.getSession().setAttribute("code", code);
        	openId = getOpenId(code);
        	request.getSession().setAttribute("openId", openId);
    	}
    	// SysWxChildMerchantToken token=sysWxChildMerchantTokenService.findByMid(mid);
    	// appId = token.getWxAppId();
    	if(openId == null || openId.length() == 0){
    		try {
    			String scheme = "https";
    			String dns = request.getServerName();
    			//生成重定向链接 实现用code换取openId，再重新跳转改方法，跳转支付页面
    			String redirectUrl = WX_RED_URL.replace("{APPID}", appId).replace("{SCHEME}", scheme).replace("{DNS}", dns)
    					.replace("{ADDR}", "subMerchantPay").replace("{STATE}", mid);
    			logger.info("redirectUrl is:" + redirectUrl);    
    			response.sendRedirect(redirectUrl);
    		} catch (IOException e) {
    			e.printStackTrace();
    			return new String("error/500: System Error!");
    		}
    		return new String("Already Redirected to WX_RED_URL!");
    	}
    	else {
    		logger.info("openId is:" + openId);
    		try {
    	    	// there is no openId, show the JSP page.
    	    	// MrtMerchantDef merchantDef=merchantDefService.findByMid(mid);
    	    	request.getSession().setAttribute("merchant", "merchantDef");
    	    	request.getSession().setAttribute("mid", mid);//用户商户号
    	    	request.getSession().setAttribute("fid", mid);//用户商户号
    	    	request.getSession().setAttribute("appId", appId);//用户公众号appId
    	    	String jspPageURL = "https" + "://" + request.getServerName() + request.getContextPath() + "/subMerchantPay.jsp";
    	    	logger.info("jspPageURL is:" + jspPageURL);    
    	    	response.sendRedirect(jspPageURL);
			} catch (IOException e) {
				e.printStackTrace();
				return new String("error/500: System Error!");
			}
	    	return "Redirected to subMerchatPay.jsp";
    	}
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Logger logger = Logger.getLogger("WeChatSubMerchantPayLog");
		logger.info("Http Get request.");
		subMerchantPay(WeChatPayOfficalAccountService.WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID,request,response);
	}
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Logger.getLogger("WeChatSubMerchantPayLog");		
		logger.info("Http Post request.");
		response.getWriter().append("WeChatPayOfficalAccountService doesn't support post!");		
	}

}
