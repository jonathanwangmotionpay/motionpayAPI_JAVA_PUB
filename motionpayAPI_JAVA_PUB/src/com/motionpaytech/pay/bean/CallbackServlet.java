package com.motionpaytech.pay.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.Provider;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.motionpaytech.fastjson.JSON;
import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.util.SignSHA1Util;
import com.motionpaytech.pay.bean.utils.RSAUtil;

/**
 * Servlet implementation class CallbackServlet
 * author Jonathan
 * get call back for payment result
 */
@WebServlet("/CallbackServlet")
public class CallbackServlet extends HttpServlet {
	/*
	 * Serialized version id
	 */
	private static final long serialVersionUID = 1L;
	private static final String LOG_FILENAME = AppConfigInitUtil.INSTANCE.getTomcatFolder() + "logs/motionpayCallback" + 
											   AppConfigInitUtil.INSTANCE.getTomcatWebAppName() + ".log";
	private static final String LOG_OUTTRADENO = AppConfigInitUtil.INSTANCE.getTomcatFolder() + "logs/motionpayOutTradeNo" +
												 AppConfigInitUtil.INSTANCE.getTomcatWebAppName() + ".log";
	private static final String SUCCESSFUL_TRANSACTIONS = AppConfigInitUtil.INSTANCE.getTomcatFolder() + "logs/motionpaySuccessTransactions" +
												 		  AppConfigInitUtil.INSTANCE.getTomcatWebAppName() + ".log";
	
	private static boolean loggerCreated = false;
	
	/**
	 * Encryption method name
	 * */
	private static final String ALGORITHOM = "RSA";
	/**
	 * default encryption provider
	 * */
	private static final Provider DEFAULT_PROVIDER = new BouncyCastleProvider();	

	public static String getTransactionsFileName() {
		return SUCCESSFUL_TRANSACTIONS;
	}
	
	public static String getOutTradeNoFileName() {
		return LOG_OUTTRADENO;
	}	
	
	public Logger getOutTradeNoLogger() {
		if (loggerCreated == false) {
			createLogger();
		}
		Logger loggerOTN = Logger.getLogger("OutTradeNo");
		return loggerOTN;
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CallbackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private void createLogger() {
    	if (loggerCreated == true)
    		return;
    	try {
	    	Logger logger = Logger.getLogger("PaymentLog");
	        FileHandler fh = new FileHandler(LOG_FILENAME, true);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);
	        logger.info("PaymentLog init");
	        
	        Logger loggerSt = Logger.getLogger("SuccessTransaction");
	        FileHandler fhSt = new FileHandler(SUCCESSFUL_TRANSACTIONS, true);  
	        loggerSt.addHandler(fhSt);
	        fhSt.setFormatter(formatter);
	        loggerSt.info("SuccessTransaction init");
	        
	        Logger loggerOTN = Logger.getLogger("OutTradeNo");
	        FileHandler fhOTN = new FileHandler(LOG_OUTTRADENO, true);  
	        loggerOTN.addHandler(fhOTN);
	        fhOTN.setFormatter(formatter);
	        loggerOTN.info("OutTradeNo init");	        

	        loggerCreated = true;
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    //Initialize global variables
    public void init() throws ServletException {
    	createLogger();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Logger logger = Logger.getLogger("PaymentLog");
		logger.info("get post request.");
		response.getWriter().append("call back doesn't support get!");
	}
	
	private String getSign(String jsonStr) {
        final JSONObject object = JSON.parseObject(jsonStr);
        object.put("sign", "");
        // System.out.println("the json obj is:"+object.toJSONString());
        final String config = (AppConfigInitUtil.INSTANCE.getAppIdOnline());
        final String config2 = (AppConfigInitUtil.INSTANCE.getAppSecretOnline());
        final String signStr = SignSHA1Util.getSignOnline(object, config, config2);
        return signStr;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Logger.getLogger("PaymentLog");
		Logger loggerSt = Logger.getLogger("SuccessTransaction");
		
		try {
            String line = "";
            String inputStr = "";
    
            logger.info("get post request in callback servlet.");
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(request.getInputStream()));

            while ((line = reader.readLine()) != null) {
            	inputStr += line;
            }
            logger.info("the contenet we got:" + inputStr);
            
            JSONObject json = JSONObject.parseObject(inputStr);           
			if(json.isEmpty() != true) {
				boolean signMatch = false;
			    JSONObject j = json;
			    String mid = j.getString("mid");
			    // String out_trade_no = j.getString("out_trade_no");
			    String pay_channel = j.getString("pay_channel");
			    String pay_result = j.getString("pay_result");
			    // String third_order_no = j.getString("third_order_no");
			    String total_fee = j.getString("total_fee");
			    String transaction_id = j.getString("transaction_id");
			    String signFromServer = j.getString("sign");
			    String signLocal = getSign(inputStr);
			    logger.info("signFromServer is:"+signFromServer);
			    logger.info("signLocal is:"+signLocal);
			    if(signFromServer != null && signFromServer.length() > 0 && signLocal != null && signLocal.length() > 0 && signFromServer.compareTo(signLocal) == 0) {
			    	signMatch = true;
			    }
			    // if (signMatch == true && mid.compareToIgnoreCase(PrePayBean.MERCHANT_ID) == 0 && pay_result.compareToIgnoreCase("SUCCESS") == 0) {
			    // remove mid checking for now. we need to put it back later.
			    if (signMatch == true && pay_result.compareToIgnoreCase("SUCCESS") == 0) {
			    	loggerSt.info(j.toJSONString());

					JSONObject jObject = new JSONObject();
					response.setContentType("application/json");
			        response.setCharacterEncoding("UTF-8");
			        jObject.put("code", "0");
			        jObject.put("message", "success");  // or success
			        String output = jObject.toJSONString();
			        logger.info("output is:" + output);
			        String es = output;
					logger.info("encrypted return message is:" + es);
			        response.getWriter().write(es);			    	
			        logger.info("write the success transaction id into file.");
			    }	        
			    else {
			    	logger.info("The call back information is invalid.");
			    }
			}            
            logger.info("exit the servlet");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.info("exception in CallbackServlet:+" + e.getMessage());
		}		
	}
}
