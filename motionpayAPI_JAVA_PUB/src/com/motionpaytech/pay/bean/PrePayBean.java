package com.motionpaytech.pay.bean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Hashtable;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.motionpaytech.fastjson.JSON;
import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.presenter.model.req.PayOnlineH5Req;
import com.motionpaytech.onlineapi.presenter.model.req.PayOnlineMobileScanReq;
import com.motionpaytech.onlineapi.presenter.model.req.PayOfflineScanMobileReq;
import com.motionpaytech.onlineapi.presenter.model.req.PayOfflineScanMobilePosReq;
import com.motionpaytech.onlineapi.presenter.model.req.PayOnlineQueryReq;
import com.motionpaytech.onlineapi.presenter.model.req.PayOnlineRefundReq;
import com.motionpaytech.onlineapi.util.SignSHA1Util;
import com.motionpaytech.onlineapi.util.TokenGenerater;
import com.motionpaytech.pay.bean.utils.HttpMethod;
import com.motionpaytech.pay.bean.utils.RSAUtil;
import com.motionpaytech.qrgen.javase.QRCode;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class PrePayBean {
	/**
	 * Encryption method name
	 * */
	private static final String ALGORITHOM = "RSA";
	/**
	 * default encryption provider
	 * */
	private static final Provider DEFAULT_PROVIDER = new BouncyCastleProvider();
	
	public static final String MERCHANT_ID = AppConfigInitUtil.INSTANCE.getMidOnline();
	private static final String SERVER_IP = "192.168.1.1";
	private static final String RETURN_URL = AppConfigInitUtil.INSTANCE.getDefaultReturnURL();
	
	public void setTomcatWebAppName(String str) {
		AppConfigInitUtil.INSTANCE.setTomcatWebAppName(str);
	}
	
	public void setReturnHostURL(String str) {
		AppConfigInitUtil.INSTANCE.setReturnHostURL(str);
	}
	
	private void saveOutTradeNo(String str, String paymentType) {
		CallbackServlet svl = new CallbackServlet();
		Logger logger = svl.getOutTradeNoLogger(); 
		logger.info("OutTradeNo:" + str + ",paymentType:" + paymentType);
	}
	
	public List<String> getOrderList() {
		List<String> list = new ArrayList<String>();
		
		try {
			String filename = com.motionpaytech.pay.bean.CallbackServlet.getOutTradeNoFileName();
			java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.FileReader(filename));
			String line = reader.readLine();
			while (line != null) {
			    // do something
			    line = reader.readLine();
			    list.add(line);
			}
			reader.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Hashtable<String, String> refundOrder(String outTradeNo, String refundAmount, String totalAmount, String paymentType) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		Hashtable<String, String> resultTable = new Hashtable<String, String>();
		
		int refund_amount = 0;
		int total_fee = 0;
		if(refundAmount != null && refundAmount.length() > 0) {
			try {
				float refundAmountInFloat = (new Float(refundAmount)).floatValue() * 100;
				refund_amount = (int) refundAmountInFloat;
			}
			catch (Exception e) {}
		}
		if(totalAmount != null && totalAmount.length() > 0) {
			try {
				float totalAmountInFloat = (new Float(totalAmount)).floatValue() * 100;
				total_fee = (int) totalAmountInFloat;
			}
			catch (Exception e) {}
		}
		
		System.out.println("the paymentType is" + paymentType);
		if(paymentType != null && (paymentType.compareToIgnoreCase("H5_A") == 0)) {
			System.out.println("set the paymentType for H5");
			AppConfigInitUtil.INSTANCE.setMidInfoForH5();
		}
		else {
			System.out.println("set the paymentType for online");
			AppConfigInitUtil.INSTANCE.setMidInfoForOnlineAPI();
		}
		
        final PayOnlineRefundReq payOnlineRefundReq = new PayOnlineRefundReq();
        payOnlineRefundReq.setOut_trade_no(outTradeNo);
        payOnlineRefundReq.setTotal_fee(total_fee);
        payOnlineRefundReq.setRefund_amount(refund_amount);
        payOnlineRefundReq.initSign();
        String JSONstr = JSONObject.toJSONString(payOnlineRefundReq);
        System.out.println("JSONstr to send:"+JSONstr);
		
        String strResult = "";
 		try {
 			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ORDER_REVOKE, JSONstr);
 			System.out.println("Return result:" + strResult);
 			if (StringUtils.isEmpty(strResult)) {
 				System.err.println("Result is empty.");
 				return resultTable;
 			}
 			JSONObject json = JSONObject.parseObject(strResult);
 			Map<String, Object> map = json.getInnerMap();
 			for (Entry<String, Object> entry : map.entrySet())
 			{
 				resultTable.put(entry.getKey(), entry.getValue().toString());
 			}
 			if(json.containsKey("content")) {
 				String content = json.getString("content");
 			    JSONObject j = JSONObject.parseObject(content);
 			    json.put("content", j);
 			    Map<String, Object> mapInner = j.getInnerMap();
 				for (Entry<String, Object> entryInner : mapInner.entrySet())
 				{
 					System.out.println("key:"+entryInner.getKey()+",value="+entryInner.getValue());
 					resultTable.put(entryInner.getKey(), entryInner.getValue().toString());
 				}
 			}
 			System.out.println("Return JSON String:" + json.toJSONString());
 			JSONObject content = (JSONObject) json.get("content");	
 			System.out.println("content is:"+content);
 		} catch (Exception e) {
 			e.printStackTrace();
 		}        		        
		return resultTable;
	}
	
	
	public Hashtable<String, String> queryOrder(String outTradeNo,String paymentType) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		if(paymentType != null && (paymentType.compareToIgnoreCase("U") == 0 || paymentType.compareTo("QR_PA") == 0 || paymentType.compareTo("QR_PW") == 0)) {
			return queryOrderOfflinePOS(outTradeNo);
		}
		else {
			return queryOrderInner(outTradeNo,paymentType);
		}
	}
	
	public Hashtable<String, String> queryOrderInner(String outTradeNo,String paymentType) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		Hashtable<String, String> resultTable = new Hashtable<String, String>();
		
		
		if(paymentType != null && (paymentType.compareToIgnoreCase("H5_A") == 0)) {
			AppConfigInitUtil.INSTANCE.setMidInfoForH5();
		}
		else {
			AppConfigInitUtil.INSTANCE.setMidInfoForOnlineAPI();
		}		
        final PayOnlineQueryReq payOnlineQueryReq = new PayOnlineQueryReq();
        payOnlineQueryReq.setOut_trade_no(outTradeNo);
        payOnlineQueryReq.initSign();
        String JSONstr = JSONObject.toJSONString(payOnlineQueryReq);
        System.out.println("JSONstr to send:"+JSONstr);
        
        String strResult = "";
		try {
			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ORDER_QUERY, JSONstr);
			System.out.println("Return result:" + strResult);
			if (StringUtils.isEmpty(strResult)) {
				System.err.println("Result is empty.");
				return resultTable;
			}
			JSONObject json = JSONObject.parseObject(strResult);
			Map<String, Object> map = json.getInnerMap();
			for (Entry<String, Object> entry : map.entrySet())
			{
				resultTable.put(entry.getKey(), entry.getValue().toString());
			}
			if(json.containsKey("content")) {
				String content = json.getString("content");
			    JSONObject j = JSONObject.parseObject(content);
			    json.put("content", j);
			    Map<String, Object> mapInner = j.getInnerMap();
				for (Entry<String, Object> entryInner : mapInner.entrySet())
				{
					System.out.println("key:"+entryInner.getKey()+",value="+entryInner.getValue());
					resultTable.put(entryInner.getKey(), entryInner.getValue().toString());
				}
			}
			System.out.println("Return JSON String:" + json.toJSONString());
			JSONObject content = (JSONObject) json.get("content");	
			System.out.println("content is:"+content);
		} catch (Exception e) {
			e.printStackTrace();
		}        		
		return resultTable;		
	}
		
	
	public String generateH5URL(String paymentType, String orderId, String terminalNo, String paymentAmount, String productName, String returnURL, String wapURL) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		String url = "";
		
		if(paymentType.indexOf("_A") > 0) {
			AppConfigInitUtil.INSTANCE.setMidInfoForH5();			
		}
		else {
			AppConfigInitUtil.INSTANCE.setMidInfoForOnlineAPI();
		}
		final PayOnlineH5Req payOnlineH5Req = new PayOnlineH5Req();
		payOnlineH5Req.setTerminal_no("WebServer");
        final String token = TokenGenerater.INSTANCE.newToken();
        System.out.println("token is:"+token);
        System.out.println("wapURL is:"+wapURL);
        String out_trade_no = token+orderId;
		
        // out_trade_no = "2017121933138";
        // returnURL = "http://www.baidu.com";
        // productName = "Third Party Payment";
        // wapURL =  "http://www.baidu.com";
        
        int total_fee = (new Integer(paymentAmount)).intValue();
        saveOutTradeNo(out_trade_no,paymentType);
        payOnlineH5Req.setOut_trade_no(out_trade_no);
        payOnlineH5Req.setTotal_fee(total_fee);
        if(paymentType != null && paymentType.length() > 0) {
        	paymentType = paymentType.replaceAll("H5_", "");
        }
        payOnlineH5Req.setPay_channel(paymentType);
        productName = productName.replace(" ", "_"); // space is not valid in the product name
        payOnlineH5Req.setGoods_info(productName);
        payOnlineH5Req.setReturn_url(returnURL);
        payOnlineH5Req.setWap_url(wapURL);
        // String out_trade_no = token;
        payOnlineH5Req.initSign();
        // String url = payOnlineZhuSaoReq.getAction();
        String JSONstr = JSONObject.toJSONString(payOnlineH5Req);
        System.out.println("JSONstr to send:"+JSONstr);
        
        String strResult;
        String qrcodeURL = "";
		try {
			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.H5_PAY, JSONstr);
			System.out.println("Return result:" + strResult);
			if (StringUtils.isEmpty(strResult)) {
				System.err.println("Result is empty.");
				return "";
			}
			JSONObject json = JSONObject.parseObject(strResult);
			System.out.println("Return JSON String:" + json.toJSONString());
			JSONObject content = (JSONObject) json.get("content");
			if(content != null) {
				qrcodeURL = content.getString("qrcode");
				System.out.println("qrcode URL is:" + qrcodeURL);
			}
            String mid = AppConfigInitUtil.INSTANCE.getMidOnline();
            Object localObject = new JSONObject();
            ((JSONObject)localObject).put("mid", mid);
            ((JSONObject)localObject).put("out_trade_no", out_trade_no);			
			String str1 = AppConfigInitUtil.INSTANCE.getAppIdOnline();
            String str2 = AppConfigInitUtil.INSTANCE.getAppSecretOnline();
            localObject = SignSHA1Util.getSignOnline((JSONObject)localObject, str1, str2);			
			url = AppConfigInitUtil.INSTANCE.GET_PAY_URL + "?mid=" + mid + "&out_trade_no=" + out_trade_no + "&sign=" + (String)localObject;
			System.out.println("return URL is:" + url + "#");	
		} catch (Exception e) {
			e.printStackTrace();
		}        				
		
		return url;
	}
	
	public String generateQRImageOnline(String paymentType, String orderId, String terminalNo, String paymentAmount, String productName, String returnURL) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		String filename = "";
		
		AppConfigInitUtil.INSTANCE.setMidInfoForOnlineAPI();
        int total_fee = (new Integer(paymentAmount)).intValue();
        final PayOnlineMobileScanReq payOnlineMobileScanReq = new PayOnlineMobileScanReq();
        final String token = TokenGenerater.INSTANCE.newToken();
        System.out.println("token is:"+token);
        saveOutTradeNo(token+orderId,paymentType);
        payOnlineMobileScanReq.setOut_trade_no(token+orderId);
        payOnlineMobileScanReq.setTotal_fee(total_fee);
        payOnlineMobileScanReq.setPay_channel(paymentType);
        productName = productName.replace(" ", "_"); // space is not valid in the product name
        payOnlineMobileScanReq.setGoods_info(productName);
        System.out.println("returnURL is:"+returnURL);
        payOnlineMobileScanReq.setReturn_url(returnURL);
        // String out_trade_no = token;
        payOnlineMobileScanReq.initSign();
        // String url = payOnlineZhuSaoReq.getAction();
        String JSONstr = JSONObject.toJSONString(payOnlineMobileScanReq);
        System.out.println("JSONstr to send:"+JSONstr);
        
        String strResult;
        String qrcodeURL = "";
		try {
			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.PRE_PAY, JSONstr);
			System.out.println("Return result:" + strResult);
			if (StringUtils.isEmpty(strResult)) {
				System.err.println("Result is empty.");
				return "";
			}
			JSONObject json = JSONObject.parseObject(strResult);
			if(json.containsKey("content")) {
				String content = json.getString("content");
			    JSONObject j = JSONObject.parseObject(content);
			    json.put("content", j);
			}
			System.out.println("Return JSON String:" + json.toJSONString());
			JSONObject content = (JSONObject) json.get("content");
			qrcodeURL = content.getString("qrcode");
			System.out.println("qrcode URL is:" + qrcodeURL);			
		} catch (Exception e) {
			e.printStackTrace();
		}        		

		if(qrcodeURL != null && qrcodeURL.length() > 0) {
		    File file = QRCode.from(qrcodeURL).withColor(0xFF000000, 0xFFFFFFFF).withSize(220, 220).file();
		    filename = "qrcode_" + orderId + ".png";
		    String imageFolder = AppConfigInitUtil.INSTANCE.getTomcatWebAppImageFolder();
		    File tempFile = new File(imageFolder + filename);
		    Files.copy(file.toPath(), new FileOutputStream(tempFile));
		    System.out.println(tempFile.getAbsoluteFile());  
		}
		return filename;		
	}

	public String scanMobilePayment(String barcode, String paymentType, String orderId, String terminalNo, String paymentAmount, String productName, String returnURL) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		String result = "";
		
		AppConfigInitUtil.INSTANCE.setMidInfoForOffline();
        int total_fee = (new Integer(paymentAmount)).intValue();
        final PayOfflineScanMobileReq payOfflineScanMobileReq = new PayOfflineScanMobileReq();
        final String token = TokenGenerater.INSTANCE.newToken();
        System.out.println("token is:"+token);
        saveOutTradeNo(token+orderId,paymentType);
        payOfflineScanMobileReq.setOut_trade_no(token+orderId);
        payOfflineScanMobileReq.setTotal_fee(total_fee);
        payOfflineScanMobileReq.setPay_channel(paymentType);
        productName = productName.replace(" ", "_"); // space is not valid in the product name
        payOfflineScanMobileReq.setGoods_info(productName);
        System.out.println("returnURL is:"+returnURL);
        payOfflineScanMobileReq.setReturn_url(returnURL);
        payOfflineScanMobileReq.setAuth_code(barcode);
        // String out_trade_no = token;
        payOfflineScanMobileReq.initSign();
        // String url = payOnlineZhuSaoReq.getAction();
        String JSONstr = JSONObject.toJSONString(payOfflineScanMobileReq);
        System.out.println("JSONstr to send:"+JSONstr);
        
        String strResult;
		try {
			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ORDER_PAY, JSONstr);
			System.out.println("Return result:" + strResult);
			if (StringUtils.isEmpty(strResult)) {
				System.err.println("Result is empty.");
				return "";
			}
			JSONObject json = JSONObject.parseObject(strResult);
			if(json.containsKey("content")) {
				String content = json.getString("content");
			    JSONObject j = JSONObject.parseObject(content);
			    json.put("content", j);
			}
			System.out.println("Return JSON String:" + json.toJSONString());
			String message = json.getString("message");
			String code = json.getString("code");
			if(code != null && code.compareTo("0") != 0) {
				result = message;
			}
			// JSONObject content = (JSONObject) json.get("content");
			// String transaction_id = content.getString("transaction_id");
			// System.out.println("transaction_id URL is:" + transaction_id);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;		
	}	
	
	public String generateQRImageOfflinePOS(String paymentType, String orderId, String terminalNo, String paymentAmount, String productName) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		String filename = "";
		
		if(paymentType == null)
			paymentType = "";
		AppConfigInitUtil.INSTANCE.setMidInfoForOffline();
        int total_fee = (new Integer(paymentAmount)).intValue();
        final String token = TokenGenerater.INSTANCE.newToken();
        System.out.println("token is:"+token);
        saveOutTradeNo(token+orderId,paymentType);
        productName = productName.replace(" ", "_"); // space is not valid in the product name		

        final PayOfflineScanMobilePosReq payOfflineScanMobilePosReq = new PayOfflineScanMobilePosReq();
        final String merchantOrderNo = token+orderId;
        payOfflineScanMobilePosReq.put("amount", total_fee);
        if(paymentType.compareToIgnoreCase("QR_PA") == 0) {
        	payOfflineScanMobilePosReq.put("payChannel", "A");
        	payOfflineScanMobilePosReq.put("flag", "alipay_native");	
        }
        else if(paymentType.compareToIgnoreCase("QR_PW") == 0) {
        	payOfflineScanMobilePosReq.put("payChannel", "W");	
        	payOfflineScanMobilePosReq.put("flag", "weixin_native");	
        }
        // payOfflineScanMobilePosReq.put("authCode", ""); // authCode must be removed for QR image 
        payOfflineScanMobilePosReq.put("merchantOrderNo", merchantOrderNo);
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("goods_info", productName);
        jsonObject.put("spbill_create_ip", "192.168.2.253");
        jsonObject.put("store_id", "");
        jsonObject.put("terminal_no", "MyPosT00001");
        payOfflineScanMobilePosReq.put("paramJsonObject", jsonObject);
        payOfflineScanMobilePosReq.initSign();
        String JSONstr = JSONObject.toJSONString(payOfflineScanMobilePosReq);
        System.out.println("JSONstr to send:"+JSONstr);
        
        String strResult;
        String qrcodeURL = "";
		try {
			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ACTION_API_ORDER, JSONstr);
			System.out.println("Return result:" + strResult);
			if (StringUtils.isEmpty(strResult)) {
				System.err.println("Result is empty.");
				return "";
			}
			JSONObject json = JSONObject.parseObject(strResult);
			if(json.containsKey("content")) {
				String content = json.getString("content");
			    JSONObject j = JSONObject.parseObject(content);
			    json.put("content", j);
			}
			System.out.println("Return JSON String:" + json.toJSONString());
			JSONObject content = (JSONObject) json.get("result");
			if(content != null) {
				qrcodeURL = content.getString("realPath");
			}
			System.out.println("qrcode URL is:" + qrcodeURL);			
		} catch (Exception e) {
			e.printStackTrace();
		}        		

		if(qrcodeURL != null && qrcodeURL.length() > 0) {
		    File file = QRCode.from(qrcodeURL).withColor(0xFF000000, 0xFFFFFFFF).withSize(220, 220).file();
		    filename = "qrcode_" + orderId + ".png";
		    String imageFolder = AppConfigInitUtil.INSTANCE.getTomcatWebAppImageFolder();
		    File tempFile = new File(imageFolder + filename);
		    Files.copy(file.toPath(), new FileOutputStream(tempFile));
		    System.out.println(tempFile.getAbsoluteFile());  
		}
		return filename;		
	}
	
	
	public String scanMobilePaymentPOS(String barcode, String paymentType, String orderId, String terminalNo, String paymentAmount, String productName) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		String result = "";
		
		System.out.println("enter scanMobilePaymentPOS function.");
		AppConfigInitUtil.INSTANCE.setMidInfoForOffline();
        int total_fee = (new Integer(paymentAmount)).intValue();
        final String token = TokenGenerater.INSTANCE.newToken();
        System.out.println("token is:"+token);
        saveOutTradeNo(token+orderId,paymentType);
        productName = productName.replace(" ", "_"); // space is not valid in the product name
        
        /* POS offline request start */
        final PayOfflineScanMobilePosReq payOfflineScanMobilePosReq = new PayOfflineScanMobilePosReq();
        final String merchantOrderNo = token+orderId;
        payOfflineScanMobilePosReq.put("amount", total_fee);
        payOfflineScanMobilePosReq.put("authCode", barcode);
        payOfflineScanMobilePosReq.put("payChannel", "U");
        payOfflineScanMobilePosReq.put("merchantOrderNo", merchantOrderNo);
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("goods_info", productName);
        jsonObject.put("spbill_create_ip", "192.168.2.253");
        jsonObject.put("store_id", "");
        jsonObject.put("terminal_no", "MyPosT00001");
        payOfflineScanMobilePosReq.put("paramJsonObject", jsonObject);
        payOfflineScanMobilePosReq.initSign();        
        String JSONstr = JSONObject.toJSONString(payOfflineScanMobilePosReq);
        System.out.println("JSONstr to send:"+JSONstr);
        /* POS offline request end */        
        
        String strResult;
		try {
			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ACTION_API_ORDER, JSONstr);
			System.out.println("Return result:" + strResult);
			if (StringUtils.isEmpty(strResult)) {
				System.err.println("Result is empty.");
				return "";
			}
			JSONObject json = JSONObject.parseObject(strResult);
			if(json.containsKey("content")) {
				String content = json.getString("content");
			    JSONObject j = JSONObject.parseObject(content);
			    json.put("content", j);
			}
			System.out.println("Return JSON String:" + json.toJSONString());
			String message = json.getString("message");
			String code = json.getString("code");
			if(code != null && code.compareTo("0") != 0) {
				result = message;
			}
			// JSONObject content = (JSONObject) json.get("content");
			// String transaction_id = content.getString("transaction_id");
			// System.out.println("transaction_id URL is:" + transaction_id);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;		
	}	
	
	public Hashtable<String, String> queryOrderOfflinePOS(String outTradeNo) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		Hashtable<String, String> resultTable = new Hashtable<String, String>();
		
		AppConfigInitUtil.INSTANCE.setMidInfoForOffline();
        /* POS offline request start */
        final PayOfflineScanMobilePosReq payOfflineScanMobilePosReq = new PayOfflineScanMobilePosReq();
        payOfflineScanMobilePosReq.put("merchantOrderNo", outTradeNo);
        payOfflineScanMobilePosReq.initSign();
        String JSONstr = JSONObject.toJSONString(payOfflineScanMobilePosReq);
        System.out.println("JSONstr to send for queryOrderOfflinePOS:"+JSONstr);
        /* POS offline request end */        
        
        String strResult = "";
		try {
			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ACTION_API_QUERY_ORDER, JSONstr);
			System.out.println("Return result:" + strResult);
			if (StringUtils.isEmpty(strResult)) {
				System.err.println("Result is empty.");
				return resultTable;
			}
			JSONObject json = JSONObject.parseObject(strResult);
			Map<String, Object> map = json.getInnerMap();
			for (Entry<String, Object> entry : map.entrySet())
			{
				resultTable.put(entry.getKey(), entry.getValue().toString());
			}
			if(json.containsKey("result")) {
				String content = json.getString("result");
			    JSONObject j = JSONObject.parseObject(content);
			    json.put("content", j);
			    Map<String, Object> mapInner = j.getInnerMap();
				for (Entry<String, Object> entryInner : mapInner.entrySet())
				{
					System.out.println("key:"+entryInner.getKey()+",value="+entryInner.getValue());
					resultTable.put(entryInner.getKey(), entryInner.getValue().toString());
				}
			}
			System.out.println("queryOrderOfflinePOS Return JSON String:" + json.toJSONString());
			JSONObject content = (JSONObject) json.get("content");	
			System.out.println("queryOrderOfflinePOS content is:"+content);
		} catch (Exception e) {
			e.printStackTrace();
		}        		
		return resultTable;		
	}
	
	public Hashtable<String, String> refundOrderOfflinePos(String tranCode,String orderNo,String tranLogId, String refundAmount, String totalAmount) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		Hashtable<String, String> resultTable = new Hashtable<String, String>();
		
		int refund_amount = 0;
		int total_fee = 0;
		if(refundAmount != null && refundAmount.length() > 0) {
			try {
				float refundAmountInFloat = (new Float(refundAmount)).floatValue() * 100;
				refund_amount = (int) refundAmountInFloat;
			}
			catch (Exception e) {}
		}
		if(totalAmount != null && totalAmount.length() > 0) {
			try {
				float totalAmountInFloat = (new Float(totalAmount)).floatValue() * 100;
				total_fee = (int) totalAmountInFloat;
			}
			catch (Exception e) {}
		}
		
		AppConfigInitUtil.INSTANCE.setMidInfoForOffline();
        /* POS offline request start */
        final PayOfflineScanMobilePosReq payOfflineScanMobilePosReq = new PayOfflineScanMobilePosReq();
        payOfflineScanMobilePosReq.put("tranCode", tranCode);
        payOfflineScanMobilePosReq.put("refundAmount", refund_amount);       
        payOfflineScanMobilePosReq.put("orderNo", orderNo);
        payOfflineScanMobilePosReq.put("tranLogId", tranLogId);
        payOfflineScanMobilePosReq.put("terminal_no", "MyPosT00001");
        payOfflineScanMobilePosReq.initSign();
        String JSONstr = JSONObject.toJSONString(payOfflineScanMobilePosReq);
        System.out.println("JSONstr to send:"+JSONstr);
        /* POS offline request end */        

        
        String strResult = "";
 		try {
 			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ACTION_API_REVOKE, JSONstr);
 			System.out.println("Return result:" + strResult);
 			if (StringUtils.isEmpty(strResult)) {
 				System.err.println("Result is empty.");
 				return resultTable;
 			}
 			JSONObject json = JSONObject.parseObject(strResult);
 			Map<String, Object> map = json.getInnerMap();
 			for (Entry<String, Object> entry : map.entrySet())
 			{
 				resultTable.put(entry.getKey(), entry.getValue().toString());
 			}
 			if(json.containsKey("result")) {
 				String content = json.getString("result");
 			    JSONObject j = JSONObject.parseObject(content);
 			    json.put("content", j);
 			    Map<String, Object> mapInner = j.getInnerMap();
 				for (Entry<String, Object> entryInner : mapInner.entrySet())
 				{
 					System.out.println("key:"+entryInner.getKey()+",value="+entryInner.getValue());
 					resultTable.put(entryInner.getKey(), entryInner.getValue().toString());
 				}
 			}
 			System.out.println("Return JSON String:" + json.toJSONString());
 			JSONObject content = (JSONObject) json.get("content");	
 			System.out.println("content is:"+content);
 		} catch (Exception e) {
 			e.printStackTrace();
 		}        		        
		return resultTable;
	}	
	
	public Hashtable<String, String> cancelOrderOfflinePos(String merchantOrderNo) 
			throws NoSuchAlgorithmException, InvalidKeySpecException, ParseException, FileNotFoundException, IOException, Exception {
		Hashtable<String, String> resultTable = new Hashtable<String, String>();
		
		Hashtable<String, String> resultTableQuery = this.queryOrderOfflinePOS(merchantOrderNo);	
		String tranCode = resultTableQuery.get("tranCode");
		String tranLogId = resultTableQuery.get("tranLogId");
		String state = resultTableQuery.get("state"); 
		String orderNo = resultTableQuery.get("orderNo");
		
		if(state.compareToIgnoreCase("5") == 0) {
			return resultTableQuery;
		}
		
		AppConfigInitUtil.INSTANCE.setMidInfoForOffline();
        /* POS offline request start */
        final PayOfflineScanMobilePosReq payOfflineScanMobilePosReq = new PayOfflineScanMobilePosReq();
        payOfflineScanMobilePosReq.put("tranCode", tranCode);
        payOfflineScanMobilePosReq.put("orderNo", orderNo);
        payOfflineScanMobilePosReq.put("tranLogId", tranLogId);
        payOfflineScanMobilePosReq.put("terminal_no", "MyPosT00001");
        payOfflineScanMobilePosReq.initSign();
        String JSONstr = JSONObject.toJSONString(payOfflineScanMobilePosReq);
        System.out.println("JSONstr to send:"+JSONstr);
        /* POS offline request end */        

        
        String strResult = "";
 		try {
 			strResult = HttpMethod.sendPost(AppConfigInitUtil.INSTANCE.ACTION_API_CANCEL, JSONstr);
 			System.out.println("Return result:" + strResult);
 			if (StringUtils.isEmpty(strResult)) {
 				System.err.println("Result is empty.");
 				return resultTable;
 			}
 			JSONObject json = JSONObject.parseObject(strResult);
 			Map<String, Object> map = json.getInnerMap();
 			for (Entry<String, Object> entry : map.entrySet())
 			{
 				resultTable.put(entry.getKey(), entry.getValue().toString());
 			}
 			if(json.containsKey("result")) {
 				String content = json.getString("result");
 			    JSONObject j = JSONObject.parseObject(content);
 			    json.put("content", j);
 			    Map<String, Object> mapInner = j.getInnerMap();
 				for (Entry<String, Object> entryInner : mapInner.entrySet())
 				{
 					System.out.println("key:"+entryInner.getKey()+",value="+entryInner.getValue());
 					resultTable.put(entryInner.getKey(), entryInner.getValue().toString());
 				}
 			}
 			System.out.println("Return JSON String:" + json.toJSONString());
 			JSONObject content = (JSONObject) json.get("content");	
 			System.out.println("content is:"+content);
 		} catch (Exception e) {
 			e.printStackTrace();
 		}        		        
		return resultTable;
	}	
		
	public static void main(String[] args) {
		PrePayBean bean = new PrePayBean();
		try {
			// String paymentType = "W"; // A for alipay, W for wechat
			String paymentType = "W"; // A for alipay, W for wechat
			
			java.util.Random random = new java.util.Random();
			long id = random.nextInt(99999999);
			String orderId = String.format("%08d", id);			
			String terminalNo = "WebServer1";
			String paymentAmount = "1";
			String productName = "Test Product";
			String fileName = "";
			
			// to generate the image in windows system. create a temp folder to save the image.
			String imageFolderName = "C:\\temp\\";
			//String imageFolderName = "C:\\bin\\eclipse-jee-oxygen-R-win32-x86_64\\eclipse\\workspace\\motionpay\\WebContent\\images\\";
			AppConfigInitUtil.INSTANCE.setTomcatWebAppImageFolder(imageFolderName);
			
			boolean testH5 = true;
			boolean testOffline = true;
			if(testOffline == true) {
				AppConfigInitUtil.INSTANCE.setMidInfoForOffline();
				String barcode = "134546567425905971";
				String result = bean.scanMobilePayment(barcode, paymentType, orderId, terminalNo, paymentAmount, productName, RETURN_URL);
				System.out.println("scan mobile payment result:" + result);
			}
			else if(testH5 == false) {
				AppConfigInitUtil.INSTANCE.setMidInfoForOnlineAPI();
				fileName = bean.generateQRImageOnline(paymentType, orderId, terminalNo, paymentAmount, productName, RETURN_URL);
				System.out.println("Generate image result:" + fileName);
			}
			else {
				paymentType = "H5_W";
				paymentType = "H5_A";
				// String wap_URL = "https://demo.motionpay.org/motionpayAPI_JAVA/paymentDone.jsp";
				String wap_URL = "https://demo.motionpay.org/motionpayAPI_JAVA/paymentDone.jsp?orderId="+orderId;
				fileName = bean.generateH5URL(paymentType, orderId, terminalNo, paymentAmount, productName, RETURN_URL, wap_URL);
				System.out.println("H5 URL result:" + fileName);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}