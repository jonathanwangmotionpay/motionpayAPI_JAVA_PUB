/**
 * 
 */
package com.motionpaytech.pay.bean;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.motionpaytech.fastjson.JSON;
import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.util.SignSHA1Util;

/**
 * Servlet implementation class WeChatPayService
 * author Jonathan
 * WeChatPayService for Mini App
 */
@WebServlet("/WeChatPayService")
public class WeChatPayService extends HttpServlet {

	/**
	 * Serialized version id
	 */
	private static final long serialVersionUID = 1L;
	private static boolean loggerCreated = false;
	private static final String LOG_FILENAME = AppConfigInitUtil.INSTANCE.getTomcatFolder() + "logs/WeChatPayService" + 
			   AppConfigInitUtil.INSTANCE.getTomcatWebAppName() + ".log";

	private static final String BASE_URL = "https://online.motionpaytech.com/onlinePayment/v1_1/pay";
	private static final String ORDER_MINI_URL = BASE_URL + "/mini";
	
    /* CNY parameters
    private static final String WECHAT_MINI_MID = ("100100010000037");
	private static final String WECHAT_MINI_APPID = ("5005642018013");
	private static final String WECHAT_MINI_APPSECURE = ("2952baf16f0df42c8ff68c1da1c3c638");
	 */
    /* CAD parameters */
	private static final String WECHAT_MINI_MID = ("100100010000025");
	private static final String WECHAT_MINI_APPID = ("5005642018001");
	private static final String WECHAT_MINI_APPSECURE = ("fdb811ec923f14f2a4c4f37435a3e451");
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WeChatPayService() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private void createLogger() {
    	if (loggerCreated == true)
    		return;
    	try {
	    	Logger logger = Logger.getLogger("WeChatPayServiceLog");
	        FileHandler fh = new FileHandler(LOG_FILENAME, true);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);
	        logger.info("PaymentLog init");       

	        loggerCreated = true;
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    //Initialize global variables
    public void init() throws ServletException {
    	createLogger();
    }
 	
	private String getSign(String jsonStr) {
        final JSONObject object = JSON.parseObject(jsonStr);
        object.put("sign", "");
        // System.out.println("the json obj is:"+object.toJSONString());
        final String signStr = SignSHA1Util.getSignOnline(object, WECHAT_MINI_APPID, WECHAT_MINI_APPSECURE);
        return signStr;
	}
	
	private String generateOutTradeNo()
	{
		String str = new SimpleDateFormat("yyyyMMdd").format(new Date()) + Math.round(Math.random() * 89999);
		return str;
	}
	
	public JSONObject prePayCanada(JSONObject inputJson) {
		String out_trade_no = generateOutTradeNo();
		String total_fee = inputJson.getString("tranAmount");
		String openid = inputJson.getString("openId");

		JSONObject requestJson = new JSONObject();
		requestJson.put("mid", WECHAT_MINI_MID);
		requestJson.put("out_trade_no", out_trade_no);
		requestJson.put("pay_channel", "W");
		requestJson.put("total_fee", total_fee);
		requestJson.put("goods_info", "ThirdPartyProduct");
		requestJson.put("return_url",  AppConfigInitUtil.INSTANCE.getDefaultReturnURL());
		requestJson.put("spbill_create_ip", "192.168.1.132");
		// requestJson.put("terminal_no", "MiniApp"); // WeChat Mini App Account later.
		requestJson.put("terminal_no", "WebServer"); // Use WebServer for now.
		requestJson.put("openid", openid);
//		SysWxChildMerchantToken token=sysWxChildMerchantTokenDao.getByMid(prePayReq.getSysParam().getMid());
//		String sign = MakeSign.getSignValue(inputJson, token.getWxAppId(), token.getWxAppSecret());
//		String sign = MakeSign.getSignValue(inputJson, "5005642018003", "a0e99280e3c9b4df6f907c7eab3e063e");
		String signLocal = getSign(requestJson.toJSONString());
		requestJson.put("sign", signLocal);
		
		//send payment request
		System.out.println("The input Json:" + requestJson);
		String result = postResult(ORDER_MINI_URL, requestJson);
		JSONObject resultJson = JSON.parseObject(result);
		System.out.println("The response Json:" + result);
		return resultJson;
	}
	
	public static String postResult(String url, JSONObject requestJson) {
		String requestParam = JSON.toJSONString(requestJson);
		String strResult = sendPost(url, requestParam);
		return strResult;
	}
	
	public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            URLConnection conn = realUrl.openConnection();

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "utf-8");  

            conn.setDoOutput(true);
            conn.setDoInput(true);

            out = new PrintWriter(conn.getOutputStream());
            out.print(param);

            out.flush();

            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }

        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Logger logger = Logger.getLogger("WeChatPayServiceLog");
		logger.info("get post request.");
		response.getWriter().append("WeChatPayService doesn't support get!");
	}
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Logger.getLogger("WeChatPayServiceLog");		
		try {
            String line = "";
            String inputStr = "";
            logger.info("get post request in WeChatPayService servlet.");
            BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));

            while ((line = reader.readLine()) != null) {
            	inputStr += line;
            }
            logger.info("the contenet we got:" + inputStr);
            
            JSONObject jsonRequest = JSONObject.parseObject(inputStr);           
			if(jsonRequest.isEmpty() != true) {
				JSONObject jsonResponse = prePayCanada(jsonRequest);			
				String es = jsonResponse.toJSONString();
				logger.info("the contenet in response:" + es);
				response.getWriter().write(es);	
			}
		    
            logger.info("exit the servlet");
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.info("exception in WeChatPayService:+" + e.getMessage());
		}			
	}
}
