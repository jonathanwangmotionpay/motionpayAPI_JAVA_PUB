package com.motionpaytech.onlineapi.presenter.model.req;

import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.net.ApiConstants;

public class PayOfflineScanMobileReq extends BaseUnderlineReq {
    private String goods_info;
    private String out_trade_no;
    private String pay_channel;
    private String return_url;
    private String spbill_create_ip;
    private String terminal_no;
    private int total_fee;
    private String auth_code;
    
    public PayOfflineScanMobileReq() {
        this.pay_channel = "U";
        this.terminal_no = "WebServer";
        this.goods_info = "Default Product";
        this.out_trade_no = "";
        this.spbill_create_ip = "192.168.1.1";
        this.return_url = AppConfigInitUtil.INSTANCE.getDefaultReturnURL();
        this.setAction(ApiConstants.INSTANCE.getACTION_API_ONLINE_PREPAY());
    }
    

    public final String getGoods_info() {
        return this.goods_info;
    }
    

    public final String getOut_trade_no() {
        return this.out_trade_no;
    }
    

    public final String getPay_channel() {
        return this.pay_channel;
    }
    

    public final String getReturn_url() {
        return this.return_url;
    }
    

    public final String getSpbill_create_ip() {
        return this.spbill_create_ip;
    }
    

    public final String getTerminal_no() {
        return this.terminal_no;
    }
    
    public final int getTotal_fee() {
        return this.total_fee;
    }
    
    public final String getAuth_code() {
        return this.auth_code;
    }
    
    public final void setGoods_info(final String goods_info) {
        this.goods_info = goods_info;
    }
    
    public final void setOut_trade_no(final String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }
    
    public final void setPay_channel(final String pay_channel) {
        this.pay_channel = pay_channel;
    }
    
    public final void setReturn_url(final String return_url) {
        this.return_url = return_url;
    }
    
    public final void setSpbill_create_ip(final String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }
    
    public final void setTerminal_no(final String terminal_no) {
        this.terminal_no = terminal_no;
    }
    
    public final void setTotal_fee(final int total_fee) {
        this.total_fee = total_fee;
    }
    
    public final void setAuth_code(final String auth_code) {
        this.auth_code = auth_code;
    }

}
