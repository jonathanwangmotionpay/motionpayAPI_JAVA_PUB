package com.motionpaytech.onlineapi.presenter.model.req;

import java.io.Serializable;
import java.util.HashMap;

import com.motionpaytech.fastjson.JSON;
import com.motionpaytech.fastjson.JSONObject;

import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.net.ApiConstants;
import com.motionpaytech.onlineapi.util.SignSHA1Util;



public class PayOfflineScanMobilePosReq implements Serializable {
	
    private HashMap<String, Object> param;
    private String signature;
    private HashMap<String, Object> suffix;
        
    public PayOfflineScanMobilePosReq() {
        this.param = new HashMap<String, Object>();
        this.suffix = new HashMap<String, Object>();
        this.signature = "";
        this.suffix.put("mid", AppConfigInitUtil.INSTANCE.getMidUnderline());    	
    }
    
    public final HashMap<String, Object> getParam() {
        return this.param;
    }
    
    public final String getSignature() throws Exception {
        return this.signature;
    }
    
    public final void initSign() throws Exception {
        final JSONObject jsonObject = new JSONObject(this.param);
    	System.out.println("JSON string is:"+JSON.toJSONString(jsonObject));

        final String appId = AppConfigInitUtil.INSTANCE.getAppIdUnderline();
        final String appSecure = AppConfigInitUtil.INSTANCE.getAppSecretUnderline();
        String signStr = SignSHA1Util.getSignUnderline(jsonObject, appId, appSecure);
        signStr = signStr.toLowerCase();
        System.out.println("signStr for offline is:"+signStr);
        this.setSignature(signStr);
    }
    
    public final HashMap<String, Object> getSuffix() {
        return this.suffix;
    }
    
    public final void put(final String s, final Object o) {
        this.param.put(s, o);
    }
    
    public final void setParam(final HashMap<String, Object> param) {
        this.param = param;
    }
    
    public final void setSignature(final String signature) {
        this.signature = signature;
    }
    
    public final void setSuffix(final HashMap<String, Object> suffix) {
        this.suffix = suffix;
    }
}




