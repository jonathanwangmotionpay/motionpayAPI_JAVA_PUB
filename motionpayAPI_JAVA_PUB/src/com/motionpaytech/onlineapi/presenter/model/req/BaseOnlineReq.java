
// 

package com.motionpaytech.onlineapi.presenter.model.req;

import java.util.Map;

import com.motionpaytech.fastjson.JSON;
import com.motionpaytech.fastjson.JSONObject;
import com.motionpaytech.onlineapi.db.AppConfigInitUtil;
import com.motionpaytech.onlineapi.util.SignSHA1Util;

public class BaseOnlineReq
{
    private String mid;
    private String signature;
    // private String action;
    
    
    public BaseOnlineReq() {
        this.mid = AppConfigInitUtil.INSTANCE.getMidOnline();
    }
    
    public final String getMid() {
        return this.mid;
    }
    
    
    public void setAction(String str) {
    	// action = str;
    }
    
    public void setSign(String str) {
    	signature = str;
    }
    
    public String getSign() {
    	return signature;
    }
    
    
    public final void initSign() throws Exception {
    	// System.out.println("JSON string is:"+JSON.toJSONString(this));
    	System.out.println("JSON string is:"+JSON.toJSONString(this));
        final JSONObject object = JSON.parseObject(JSON.toJSONString(this));
        final String config = (AppConfigInitUtil.INSTANCE.getAppIdOnline());
        final String config2 = (AppConfigInitUtil.INSTANCE.getAppSecretOnline());
        final String signStr = SignSHA1Util.getSignOnline(object, config, config2);
        this.setSign(signStr);
    }
    
    public final void setMid(final String mid) {
        this.mid = mid;
    }
}
