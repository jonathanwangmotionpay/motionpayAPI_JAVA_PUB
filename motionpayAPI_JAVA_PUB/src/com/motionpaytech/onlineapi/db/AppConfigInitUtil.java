package com.motionpaytech.onlineapi.db;

/**
 * TODO
 * Created by Song on 2017/11/3.
 */
public final class AppConfigInitUtil
{
	public static AppConfigInitUtil INSTANCE = null;
	
    private static String appIdOnline = "appIdOnline";

    private static String appIdUnderline = "appIdUnderline";

    private static String appSecretOnline = "appSecretOnline";

    private static String appSecretUnderline = "appSecretUnderline";

    private static String midOnline = "midOnline";

    private static String midUnderline = "midUnderline";
    
    private static String tomcatFolder = "tomcatFolder";
    
    private static String tomcatWebAppImageFolder = "tomcatWebAppFolder";
    
    private static String tomcatWebAppName = "motionpayAPI_JAVA_PUB";
    
    private static String returnHostURL = "https://demo.motionpay.org/";
    
    private static String defaultReturnURL = returnHostURL + tomcatWebAppName + "/CallbackServlet";
    
    private static String apiHostURL = "https://api.motionpay.org/";
    
    /**
     * offline POS machine API action target for order
     */
    public static String ACTION_API_ORDER = apiHostURL + "payment/pay/order";
    /**
     * offline POS machine API action target for query
     */
    public static String ACTION_API_QUERY_ORDER = apiHostURL + "payment/pay/queryOrder";
    /**
     * offline POS machine API action target for refund
     */
    public static String ACTION_API_REVOKE = apiHostURL + "payment/pay/revoke";
    /**
     * offline POS machine API action target for cancel
     */
    public static String ACTION_API_CANCEL = apiHostURL + "payment/pay/cancel";

    
	/**
	 * scan mobile
	 */
    public static String ORDER_PAY = apiHostURL + "onlinePayment/v1_1/pay/orderPay";
	/**
	 * mobile scan
	 */
    public static String PRE_PAY = apiHostURL + "onlinePayment/v1_1/pay/prePay";	
	/**
	 * H5 payment
	 */
    public static String H5_PAY = apiHostURL + "onlinePayment/v1_1/pay/wapPay";
	/**
	 * Get Pay URL
	 */
    public static String GET_PAY_URL = apiHostURL + "onlinePayment/v1_1/pay/getPayUrl";
	
	/**
	 * query
	 */
    public static String ORDER_QUERY = apiHostURL + "onlinePayment/v1_1/pay/orderQuery";
	/**
	 * refund
	 */
    public static String ORDER_REVOKE = apiHostURL + "onlinePayment/v1_1/pay/revoke";    

    static
    {
    	INSTANCE = new AppConfigInitUtil();
    }

    private AppConfigInitUtil()
    {
        initSettings();
    }


    public final String getAppIdOnline()
    {
        return appIdOnline;
    }


    public final String getAppIdUnderline()
    {
        return appIdUnderline;
    }


    public final String getAppSecretOnline()
    {
        return appSecretOnline;
    }


    public final String getAppSecretUnderline()
    {
        return appSecretUnderline;
    }


    public final String getMidOnline()
    {
        return midOnline;
    }


    public final String getMidUnderline()
    {
        return midUnderline;
    }
    
    public final String getTomcatFolder()
    {
    	return tomcatFolder;
    }
    
    public final String getTomcatWebAppImageFolder()
    {
    	return tomcatWebAppImageFolder;
    }

    public final String getDefaultReturnURL()
    {
    	return defaultReturnURL;
    }
    
    
    public void setMidInfoForH5() 
    {
    	// Merchant ID: 100100010000009
    	// Appid: 5005642017007
    	// AppSecret: 052366873962708184168aaa57a32295
    	/*
        midOnline = "100100010000009"; 
        appIdOnline = "5005642017007";
        appSecretOnline = "052366873962708184168aaa57a32295"; */
        midOnline = "100122220000004"; 
        appIdOnline = "5005642019002";
        appSecretOnline = "d228f2081e4c739d877affc448ac7d71";
    }
    
    public void setMidInfoForOnlineAPI() 
    {
    	// Merchant ID: 100105100000011
    	// Appid: 5005642017006
    	// AppSecret: 2ce9f51261e21ba6a087ee239160f0b1    	
    	
    	
    	midOnline = "100105100000011";
        appIdOnline = "5005642017006";
        appSecretOnline = "2ce9f51261e21ba6a087ee239160f0b1";
    }
    
    public void setMidInfoForOffline()
    {
    	// Merchant ID: 100100010000010
    	// Appid: 5005642017008
    	// AppSecret: 9f038a244e7fccdfe448221a1ce1c0cd
    	// updated as merchantid conflicted with live server
        midUnderline = "100122220000005";
        appIdUnderline = "5005642019001";
        appSecretUnderline = "ae26b1841a7291379db606e41bfa4417";
    }
    
    private void setTomcatDefaultFolder() 
    {
    	String catalina_home_env = System.getenv("CATALINA_HOME");
    	String catalina_home = "/home/tomcat/tomcat7";
    	if(catalina_home_env != null && catalina_home_env.length() > 0) {
    		catalina_home = catalina_home_env;
    	}
    	setTomcatFolder(catalina_home + "/");
    	setTomcatWebAppImageFolder(catalina_home + "/webapps/" + tomcatWebAppName + "/images/");
    	setDefaultReturnURL("https://demo.motionpay.org/" + tomcatWebAppName + "/CallbackServlet");
    }

    private void setTomcatFolder(String str) 
    {
    	tomcatFolder = str;
    }

    public String getTomcatWebAppName()
    {
    	return tomcatWebAppName;
    }
    
    public void setTomcatWebAppName(String str)
    {
    	tomcatWebAppName = str;
    	setTomcatDefaultFolder();
    }
    
    public void setTomcatWebAppImageFolder(String str) 
    {
    	tomcatWebAppImageFolder  = str;
    }
    
    public void setDefaultReturnURL(String str)
    {
    	defaultReturnURL = str;
    }

    public void setReturnHostURL(String str)
    {
    	returnHostURL = str;
    	setDefaultReturnURL(returnHostURL + tomcatWebAppName + "/CallbackServlet");
    }

    public void initSettings()
    {
    	setMidInfoForH5();
    	setMidInfoForOnlineAPI();
    	setMidInfoForOffline();
    	setTomcatDefaultFolder();
    }

}