package com.motionpaytech.onlineapi.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;

/**
 * TODO
 * Created by Song on 2017/11/2.
 */

public class QrCodeUtil {

    public static BufferedImage  genQRCodeTow(String content, int bitmapSize)
    {
        // 生成二维矩阵,编码时指定大小,不要生成了图片以后再进行缩放,这样会模糊导致识别失败
        try {
            Hashtable hints = new Hashtable();
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            BitMatrix matrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, bitmapSize, bitmapSize,hints);
            int width = matrix.getWidth();
            int height = matrix.getHeight();
            // 二维矩阵转为一维像素数组,也就是一直横着排了，注意添加白色背景，否则打印会是黑色。
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (matrix.get(x, y)) {
                        pixels[y * width + x] = 0xFF000000;
                    } else {// 二维码背景白色
                        pixels[y * width + x] = 0xFFFFFFFF;
                    }
                }
            }
            // BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            img.getRaster().setPixels(0, 0, width, height, pixels);
            
            // Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            // 通过像素数组生成bitmap,具体参考api
            // bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

            return img;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
