package com.motionpaytech.qrgen.core.exception;

public class QRGenerationException extends RuntimeException {
	static final long serialVersionUID = -7034897190745766939L;
    public QRGenerationException(String message, Throwable underlyingException) {
        super(message, underlyingException);
    }
}
