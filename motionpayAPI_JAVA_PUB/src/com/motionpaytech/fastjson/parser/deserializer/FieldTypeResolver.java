package com.motionpaytech.fastjson.parser.deserializer;

import java.lang.reflect.Type;

import com.motionpaytech.fastjson.parser.deserializer.ParseProcess;

public interface FieldTypeResolver extends ParseProcess {
    Type resolve(Object object, String fieldName);
}
